#include <stdio.h>
#include <stdlib.h>

int main()
{

    int i = 0;
    int temp = 0;
    int v[10];

    while(i < 10){
        scanf("%d", &temp);

        for(int j = 0; j < i; j++){
            if(temp == v[j]){
                while(temp == v[j]){
                    printf("Digite outro valor");
                    scanf("%d", &temp);
                }
                j = 0;
                continue;
            }
        }

        v[i] = temp;
        i++;
    }

    for(int i = 0; i < 10; i++) printf("%d", v[i]);

    return 0;
}
